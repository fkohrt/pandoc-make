# ---------------------------
# Makefile for Pandoc project
# ---------------------------

# SET FOLDER NAMES

TEMP_DIR ?= tmp
ASSETS_DIR ?= assets
CONTENT_DIR ?= content
CONTENT_ASSETS_DIR ?= assets

# SET PANDOC OPTIONS

SELF_CONTAINED ?= false
EXTRACT_MEDIA ?= false
SLIDE_LEVEL ?= 2
HAS_NOTES ?= true
REVEALJS_URL ?= https://unpkg.com/reveal.js@^4/
NOTES_SERVER_URL ?= https://unpkg.com/reveal-notes-server
MULTIPLEX_SOCKET_URL ?= https://reveal-multiplex.glitch.me
MULTIPLEX_SOCKET_ID ?= MULTIPLEX_SOCKET_ID # set this when calling make
MULTIPLEX_SOCKET_SECRET ?= MULTIPLEX_SOCKET_SECRET # set this when calling make
TOC ?= false
TOC_DEPTH ?= 1
INCREMENTAL ?= true
NUMBER_SECTIONS ?= false

# Source format
PD_FROM ?= markdown

# Location of your working bibliography file (relative to CONTENT_DIR)
BIB ?= bibliography.bib

# citation style (relative to ASSETS_DIR/csl/)
# if not existent will be downloaded from github.com/citation-style-language/styles-distribution
CSL ?= deutsche-gesellschaft-fur-psychologie.csl

# HTML stylesheet (relative to ASSETS_DIR/css/)
STYLE ?= holiday.css

# Comment out for no highlight; view available options with `pandoc --list-highlight-styles`
HIGHLIGHT_STYLE ?= tango

# view available themes at https://github.com/BafS/Gutenberg
GUTENBERG_THEME ?= oldstyle

# limit Pandoc's memory usage
PD_MEM_LIM ?= +RTS -M512M -RTS

# AUTOMATED PART OF MAKEFILE

ifdef HIGHLIGHT_STYLE
PD_HIGHLIGHT = --highlight-style=$(HIGHLIGHT_STYLE)
else
PD_HIGHLIGHT = --no-highlight
endif

ifeq ($(SELF_CONTAINED), true)
PD_SELF_CONTAINED = --self-contained
else
PD_SELF_CONTAINED = 
endif

ifeq ($(HAS_NOTES), true)
PD_HAS_NOTES = --variable=hasNotes
else
PD_HAS_NOTES = 
endif

ifeq ($(EXTRACT_MEDIA), true)
PD_EXTRACT_MEDIA = --extract-media=.
else
PD_EXTRACT_MEDIA = 
endif

ifeq ($(TOC), true)
PD_TOC = --table-of-contents --toc-depth=$(TOC_DEPTH)
else
PD_TOC = 
endif

ifeq ($(INCREMENTAL), true)
PD_INCREMENTAL = --incremental
else
PD_INCREMENTAL = 
endif

ifeq ($(NUMBER_SECTIONS), true)
PD_NUMBER_SECTIONS = --number-sections=true
else
PD_NUMBER_SECTIONS = --number-sections=false
endif

SRC = $(wildcard $(CONTENT_DIR)/*.md)
PDF=$(SRC:%.md=%.pdf)
TEX=$(SRC:%.md=%.tex)
HTML=$(SRC:%.md=%.html)
ODT=$(SRC:%.md=%.odt)
DOCX=$(SRC:%.md=%.docx)
REVEALJS=$(SRC:%.md=%.revealjs.html)
REVEALJS_MASTER=$(SRC:%.md=%.revealjs.master.html)
REVEALJS_CLIENT=$(SRC:%.md=%.revealjs.client.html)
BEAMER=$(SRC:%.md=%.beamer.pdf)
PPTX=$(SRC:%.md=%.pptx)

.PHONY: all pdf tex html odt docx revealjs beamer pptx clean
.PRECIOUS: $(ASSETS_DIR)%/. $(CONTENT_DIR)%/.
.SECONDARY: $(CONTENT_DIR)/$(CONTENT_ASSETS_DIR)/$(STYLE) $(ASSETS_DIR)/csl/$(CSL)

all: $(PDF) $(TEX) $(HTML) $(ODT) $(DOCX) $(REVEALJS) $(REVEALJS_MASTER) $(REVEALJS_CLIENT) $(BEAMER) $(PPTX)

pdf: $(PDF)
tex: $(TEX)
html: $(HTML)
odt: $(ODT)
docx: $(DOCX)
revealjs: $(REVEALJS) $(REVEALJS_MASTER) $(REVEALJS_CLIENT)
beamer: $(BEAMER)
pptx: $(PPTX)

$(CONTENT_DIR)/%.html: $(CONTENT_DIR)/%.md $(CONTENT_DIR)/$(CONTENT_ASSETS_DIR)/$(STYLE) $(CONTENT_DIR)/$(CONTENT_ASSETS_DIR)/print.css $(TEMP_DIR)/cito.lua $(TEMP_DIR)/pandoc-quotes.lua $(ASSETS_DIR)/csl/$(CSL) $(CONTENT_DIR)/$(BIB)
	cd $(CONTENT_DIR) && \
	pandoc $(PD_MEM_LIM) \
	--from=$(PD_FROM) \
	--to=html5 \
	--standalone \
	--css=$(CONTENT_ASSETS_DIR)/$(STYLE) \
	--css=$(CONTENT_ASSETS_DIR)/print.css \
	--lua-filter="$(CURDIR)/$(TEMP_DIR)/cito.lua" \
	--lua-filter="$(CURDIR)/$(TEMP_DIR)/pandoc-quotes.lua" \
	--citeproc \
	--csl="$(CURDIR)/$(ASSETS_DIR)/csl/$(CSL)" \
	--bibliography="$(BIB)" \
	$(PD_TOC) \
	$(PD_NUMBER_SECTIONS) \
	--output=$(@F) \
	$(PD_HIGHLIGHT) \
	$(PD_SELF_CONTAINED) \
	$(PD_EXTRACT_MEDIA) \
	$(<F)

$(CONTENT_DIR)/%.pdf: $(CONTENT_DIR)/%.md $(TEMP_DIR)/cito.lua $(ASSETS_DIR)/csl/$(CSL) $(CONTENT_DIR)/$(BIB) $(ASSETS_DIR)/metadata/latex.yml
	cd $(CONTENT_DIR) && \
	pandoc $(PD_MEM_LIM) \
	--from=$(PD_FROM) \
	--pdf-engine=lualatex \
	--standalone \
	--listings \
	--lua-filter="$(CURDIR)/$(TEMP_DIR)/cito.lua" \
	--citeproc \
	--csl="$(CURDIR)/$(ASSETS_DIR)/csl/$(CSL)" \
	--bibliography="$(BIB)" \
	$(PD_TOC) \
	$(PD_NUMBER_SECTIONS) \
	--output=$(@F) \
	--metadata-file="$(CURDIR)/$(ASSETS_DIR)/metadata/latex.yml" \
	$(PD_HIGHLIGHT) \
	$(PD_EXTRACT_MEDIA) \
	$(<F)

$(CONTENT_DIR)/%.tex: $(CONTENT_DIR)/%.md $(TEMP_DIR)/cito.lua $(ASSETS_DIR)/csl/$(CSL) $(CONTENT_DIR)/$(BIB) $(ASSETS_DIR)/metadata/latex.yml
	cd $(CONTENT_DIR) && \
	pandoc $(PD_MEM_LIM) \
	--from=$(PD_FROM) \
	--standalone \
	--listings \
	--lua-filter="$(CURDIR)/$(TEMP_DIR)/cito.lua" \
	--citeproc \
	--csl="$(CURDIR)/$(ASSETS_DIR)/csl/$(CSL)" \
	--bibliography="$(BIB)" \
	$(PD_TOC) \
	$(PD_NUMBER_SECTIONS) \
	--output=$(@F) \
	--metadata-file="$(CURDIR)/$(ASSETS_DIR)/metadata/latex.yml" \
	$(PD_HIGHLIGHT) \
	$(PD_EXTRACT_MEDIA) \
	$(<F)

$(CONTENT_DIR)/%.odt: $(CONTENT_DIR)/%.md $(TEMP_DIR)/cito.lua $(ASSETS_DIR)/csl/$(CSL) $(CONTENT_DIR)/$(BIB)
	cd $(CONTENT_DIR) && \
	pandoc $(PD_MEM_LIM) \
	--from=$(PD_FROM) \
	--to=odt \
	--lua-filter="$(CURDIR)/$(TEMP_DIR)/cito.lua" \
	--citeproc \
	--csl="$(CURDIR)/$(ASSETS_DIR)/csl/$(CSL)" \
	--bibliography="$(BIB)" \
	$(PD_TOC) \
	$(PD_NUMBER_SECTIONS) \
	--output=$(@F) \
	$(PD_HIGHLIGHT) \
	$(PD_EXTRACT_MEDIA) \
	$(<F)

$(CONTENT_DIR)/%.docx: $(CONTENT_DIR)/%.md $(TEMP_DIR)/cito.lua $(ASSETS_DIR)/csl/$(CSL) $(CONTENT_DIR)/$(BIB)
	cd $(CONTENT_DIR) && \
	pandoc $(PD_MEM_LIM) \
	--from=$(PD_FROM) \
	--to=docx \
	--lua-filter="$(CURDIR)/$(TEMP_DIR)/cito.lua" \
	--citeproc \
	--csl="$(CURDIR)/$(ASSETS_DIR)/csl/$(CSL)" \
	--bibliography="$(BIB)" \
	$(PD_TOC) \
	$(PD_NUMBER_SECTIONS) \
	--output=$(@F) \
	$(PD_HIGHLIGHT) \
	$(PD_EXTRACT_MEDIA) \
	$(<F)

$(CONTENT_DIR)/%.revealjs.html: $(CONTENT_DIR)/%.md $(CURDIR)/$(ASSETS_DIR)/templates/header-include.revealjs $(ASSETS_DIR)/templates/default.revealjs $(TEMP_DIR)/cito.lua $(TEMP_DIR)/pandoc-quotes.lua $(ASSETS_DIR)/csl/$(CSL) $(CONTENT_DIR)/$(BIB) $(ASSETS_DIR)/metadata/revealjs.yml
	cd $(CONTENT_DIR) && \
	pandoc $(PD_MEM_LIM) \
	--from=$(PD_FROM) \
	--to=revealjs \
	$(PD_INCREMENTAL) \
	--slide-level=$(SLIDE_LEVEL) \
	--standalone \
	$(PD_HAS_NOTES) \
	--include-in-header="$(CURDIR)/$(ASSETS_DIR)/templates/header-include.revealjs" \
	--template="$(CURDIR)/$(ASSETS_DIR)/templates/default.revealjs" \
	--lua-filter="$(CURDIR)/$(TEMP_DIR)/cito.lua" \
	--lua-filter="$(CURDIR)/$(TEMP_DIR)/pandoc-quotes.lua" \
	--citeproc \
	--csl="$(CURDIR)/$(ASSETS_DIR)/csl/$(CSL)" \
	--bibliography="$(BIB)" \
	$(PD_TOC) \
	$(PD_NUMBER_SECTIONS) \
	--output=$(@F) \
	--metadata-file="$(CURDIR)/$(ASSETS_DIR)/metadata/revealjs.yml" \
	--variable=revealjs-url:"$(REVEALJS_URL)" \
	$(PD_HIGHLIGHT) \
	$(PD_SELF_CONTAINED) \
	$(PD_EXTRACT_MEDIA) \
	$(<F)

$(CONTENT_DIR)/%.revealjs.master.html: $(CONTENT_DIR)/%.md $(CURDIR)/$(ASSETS_DIR)/templates/header-include.revealjs.master $(ASSETS_DIR)/templates/default.revealjs.master $(TEMP_DIR)/cito.lua $(TEMP_DIR)/pandoc-quotes.lua $(ASSETS_DIR)/csl/$(CSL) $(CONTENT_DIR)/$(BIB) $(ASSETS_DIR)/metadata/revealjs.yml
	cd $(CONTENT_DIR) && \
	pandoc $(PD_MEM_LIM) \
	--from=$(PD_FROM) \
	--to=revealjs \
	$(PD_INCREMENTAL) \
	--slide-level=$(SLIDE_LEVEL) \
	--standalone \
	$(PD_HAS_NOTES) \
	--variable=notesServerURL:$(NOTES_SERVER_URL) \
	--variable=multiplexSocketURL:$(MULTIPLEX_SOCKET_URL) \
	--variable=multiplexSocketID:$(MULTIPLEX_SOCKET_ID) \
	--variable=multiplexSocketSecret:$(MULTIPLEX_SOCKET_SECRET) \
	--include-in-header="$(CURDIR)/$(ASSETS_DIR)/templates/header-include.revealjs.master" \
	--template="$(CURDIR)/$(ASSETS_DIR)/templates/default.revealjs.master" \
	--lua-filter="$(CURDIR)/$(TEMP_DIR)/cito.lua" \
	--lua-filter="$(CURDIR)/$(TEMP_DIR)/pandoc-quotes.lua" \
	--citeproc \
	--csl="$(CURDIR)/$(ASSETS_DIR)/csl/$(CSL)" \
	--bibliography="$(BIB)" \
	$(PD_TOC) \
	$(PD_NUMBER_SECTIONS) \
	--output=$(@F) \
	--metadata-file="$(CURDIR)/$(ASSETS_DIR)/metadata/revealjs.yml" \
	--variable=revealjs-url:"$(REVEALJS_URL)" \
	$(PD_HIGHLIGHT) \
	$(PD_SELF_CONTAINED) \
	$(PD_EXTRACT_MEDIA) \
	$(<F)

$(CONTENT_DIR)/%.revealjs.client.html: $(CONTENT_DIR)/%.md $(CURDIR)/$(ASSETS_DIR)/templates/header-include.revealjs.client $(ASSETS_DIR)/templates/default.revealjs.client $(TEMP_DIR)/cito.lua $(TEMP_DIR)/pandoc-quotes.lua $(ASSETS_DIR)/csl/$(CSL) $(CONTENT_DIR)/$(BIB) $(ASSETS_DIR)/metadata/revealjs.yml
	cd $(CONTENT_DIR) && \
	pandoc $(PD_MEM_LIM) \
	--from=$(PD_FROM) \
	--to=revealjs \
	$(PD_INCREMENTAL) \
	--slide-level=$(SLIDE_LEVEL) \
	--standalone \
	$(PD_HAS_NOTES) \
	--variable=multiplexSocketURL:$(MULTIPLEX_SOCKET_URL) \
	--variable=multiplexSocketID:$(MULTIPLEX_SOCKET_ID) \
	--variable=multiplexSocketSecret:$(MULTIPLEX_SOCKET_SECRET) \
	--include-in-header="$(CURDIR)/$(ASSETS_DIR)/templates/header-include.revealjs.client" \
	--template="$(CURDIR)/$(ASSETS_DIR)/templates/default.revealjs.client" \
	--lua-filter="$(CURDIR)/$(TEMP_DIR)/cito.lua" \
	--lua-filter="$(CURDIR)/$(TEMP_DIR)/pandoc-quotes.lua" \
	--citeproc \
	--csl="$(CURDIR)/$(ASSETS_DIR)/csl/$(CSL)" \
	--bibliography="$(BIB)" \
	$(PD_TOC) \
	$(PD_NUMBER_SECTIONS) \
	--output=$(@F) \
	--metadata-file="$(CURDIR)/$(ASSETS_DIR)/metadata/revealjs.yml" \
	--variable=revealjs-url:"$(REVEALJS_URL)" \
	$(PD_HIGHLIGHT) \
	$(PD_SELF_CONTAINED) \
	$(PD_EXTRACT_MEDIA) \
	$(<F)

$(CONTENT_DIR)/%.beamer.pdf: $(CONTENT_DIR)/%.md $(TEMP_DIR)/cito.lua $(TEMP_DIR)/pandoc-quotes.lua $(ASSETS_DIR)/csl/$(CSL) $(CONTENT_DIR)/$(BIB) $(ASSETS_DIR)/metadata/beamer.yml
	cd $(CONTENT_DIR) && \
	pandoc $(PD_MEM_LIM) \
	--from=$(PD_FROM) \
	--to=beamer \
	$(PD_INCREMENTAL) \
	--slide-level=$(SLIDE_LEVEL) \
	--pdf-engine=lualatex \
	--standalone \
	--listings \
	--lua-filter="$(CURDIR)/$(TEMP_DIR)/cito.lua" \
	--citeproc \
	--csl="$(CURDIR)/$(ASSETS_DIR)/csl/$(CSL)" \
	--bibliography="$(BIB)" \
	$(PD_TOC) \
	$(PD_NUMBER_SECTIONS) \
	--output=$(@F) \
	--metadata-file="$(CURDIR)/$(ASSETS_DIR)/metadata/beamer.yml" \
	$(PD_HIGHLIGHT) \
	$(PD_EXTRACT_MEDIA) \
	$(<F)

$(CONTENT_DIR)/%.pptx: $(CONTENT_DIR)/%.md $(TEMP_DIR)/cito.lua $(ASSETS_DIR)/csl/$(CSL) $(CONTENT_DIR)/$(BIB)
	cd $(CONTENT_DIR) && \
	pandoc $(PD_MEM_LIM) \
	--from=$(PD_FROM) \
	--to=pptx \
	$(PD_INCREMENTAL) \
	--slide-level=$(SLIDE_LEVEL) \
	--lua-filter="$(CURDIR)/$(TEMP_DIR)/cito.lua" \
	--citeproc \
	--csl="$(CURDIR)/$(ASSETS_DIR)/csl/$(CSL)" \
	--bibliography="$(BIB)" \
	$(PD_TOC) \
	$(PD_NUMBER_SECTIONS) \
	--output=$(@F) \
	$(PD_HIGHLIGHT) \
	$(PD_EXTRACT_MEDIA) \
	$(<F)

clean:
	rm -rf $(TEMP_DIR)/
	rm -f $(PDF) $(HTML) $(ODT) $(DOCX) $(REVEALJS) $(REVEALJS_MASTER) $(REVEALJS_CLIENT) $(BEAMER) $(PPTX)
	rm -f $(CONTENT_DIR)/$(CONTENT_ASSETS_DIR)/$(STYLE)
	rm -f $(CONTENT_DIR)/$(CONTENT_ASSETS_DIR)/print.css
	[ "$$(ls -A $(CONTENT_ASSETS_DIR))" ] && echo "not empty" || rm -rf $(CONTENT_ASSETS_DIR)
	rm -f $(ASSETS_DIR)/templates/default.revealjs.original
	rm -f $(ASSETS_DIR)/templates/default.revealjs
	rm -f $(ASSETS_DIR)/templates/default.revealjs.master
	rm -f $(ASSETS_DIR)/templates/default.revealjs.client
	rm -f $(ASSETS_DIR)/templates/styles.html

# Empty files

$(CONTENT_DIR)/$(BIB):
	@echo creating empty bibliography file $@
	touch $@

# for automatic directory creation, see https://ismail.badawi.io/blog/2017/03/28/automatic-directory-creation-in-make/

$(ASSETS_DIR)%/.:
	mkdir -p $@

$(CONTENT_DIR)%/.:
	mkdir -p $@

$(TEMP_DIR)/.:
	mkdir -p $@

.SECONDEXPANSION:

# Build assets

$(ASSETS_DIR)/templates/default.revealjs.original: $(ASSETS_DIR)/templates/styles.html
	wget -O $@ https://github.com/jgm/pandoc-templates/raw/master/default.revealjs

$(ASSETS_DIR)/templates/default.revealjs: $(ASSETS_DIR)/templates/default.revealjs.patch $(ASSETS_DIR)/templates/default.revealjs.original
	patch --forward --reject-file=- --no-backup-if-mismatch --output=$@ $(ASSETS_DIR)/templates/default.revealjs.original $<

$(ASSETS_DIR)/templates/default.revealjs.master: $(ASSETS_DIR)/templates/default.revealjs.master.patch $(ASSETS_DIR)/templates/default.revealjs.original
	patch --forward --reject-file=- --no-backup-if-mismatch --output=$@ $(ASSETS_DIR)/templates/default.revealjs.original $<

$(ASSETS_DIR)/templates/default.revealjs.client: $(ASSETS_DIR)/templates/default.revealjs.client.patch $(ASSETS_DIR)/templates/default.revealjs.original
	patch --forward --reject-file=- --no-backup-if-mismatch --output=$@ $(ASSETS_DIR)/templates/default.revealjs.original $<

$(ASSETS_DIR)/templates/styles.html:
	wget -O $@ https://github.com/jgm/pandoc-templates/raw/master/$(@F)

$(CONTENT_DIR)/$(CONTENT_ASSETS_DIR)/%.css: $(ASSETS_DIR)/css/%.css | $$(@D)/.
	echo "@media screen {" > $@
	cat $< >> $@
	echo "}" >> $@

$(CONTENT_DIR)/$(CONTENT_ASSETS_DIR)/print.css: $(ASSETS_DIR)/css/gutenberg.css $(ASSETS_DIR)/css/gutenberg-theme.css | $$(@D)/.
	echo "@media print {" > $@
	cat $(ASSETS_DIR)/css/gutenberg.css >> $@
	cat $(ASSETS_DIR)/css/gutenberg-theme.css >> $@
	echo "}" >> $@

# Remote assets

$(ASSETS_DIR)/csl/%.csl: | $$(@D)/.
ifeq (,$(wildcard $@))
	wget -O $@ https://github.com/citation-style-language/styles-distribution/raw/master/$(@F)
endif

$(TEMP_DIR)/pandoc-quotes.lua: | $$(@D)/.
	wget -O $@ https://github.com/pandoc/lua-filters/raw/master/pandoc-quotes.lua/pandoc-quotes.lua

$(TEMP_DIR)/cito.lua: | $$(@D)/.
	wget -O $@ https://github.com/pandoc/lua-filters/raw/master/cito/cito.lua

$(ASSETS_DIR)/css/gutenberg.css: | $$(@D)/.
ifeq (,$(wildcard $@))
	wget -O $@ https://unpkg.com/gutenberg-css@0.6.0/dist/gutenberg.min.css
endif

$(ASSETS_DIR)/css/gutenberg-theme.css: | $$(@D)/.
ifeq (,$(wildcard $@))
	wget -O $@ https://unpkg.com/gutenberg-css@0.6.0/dist/themes/$(GUTENBERG_THEME).min.css
endif
